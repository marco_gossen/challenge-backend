package com.challenge.marco_project.algorithmsChallenge;

import org.springframework.stereotype.Component;

import java.util.HashMap;

/**
 * Created by marco on 22/09/18.
 */
@Component
public class Fibonacci {

    public HashMap<Long, Long[]> combinations;

    public Long[] fibonacci(long n){
        if(combinations != null && combinations.containsKey(n)){
            return combinations.get(n);
        }else{
            combinations = new HashMap<Long, Long[]>();
        }

        Long n1 = 0L;
        Long n2 = 1L;
        Long[] array = new Long[92];
        if(n == 0){
            combinations.put(n, array);
            return combinations.get(n);
        }
        if(n == 1){
            array[0] = n1;
            combinations.put(n, array);
            return combinations.get(n);
        }
        array[0] = n1;
        array[1]= n2;
        int largo = 2;
        while(true){
            Long aux = n1;
            n1 = n2;
            n2 = aux + n1;
            if(n2 > n){
                break;
            }
            array[largo] = n2;
            largo++;
        }
        combinations.put(n, array);
        return combinations.get(n);
    }
}
