package com.challenge.marco_project.repository;

import com.challenge.marco_project.model.VitalSigns;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by marco on 21/09/18.
 */
@Repository
public interface VitalSignRepository extends JpaRepository<VitalSigns, Long> {
    List<VitalSigns> findByPatientId(Long patientId);

    @Query("SELECT d from VitalSigns d where d.patient.id = :patientId and d.updatedAt = (select max(f.updatedAt) from VitalSigns f where f.patient.id = :patientId)")
    VitalSigns findLatestVitalSigns(@Param("patientId") Long patientId);
}
